package ua.com.university.java;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.CommonDataSource;
import javax.sql.DataSource;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import javax.sql.rowset.WebRowSet;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/**
 * SLF4J - Logging
 * Use slf4j logback is native implementation
 * There are also SLF4J bindings external to the SLF4J project, e.g. logback which implements SLF4J natively. 
 * Logback's ch.qos.logback.classic.Logger class is a direct implementation of SLF4J's org.slf4j.Logger interface. 
 * Thus, using SLF4J in conjunction with logback involves strictly zero memory and computational overhead. 
 *
 */
public class App {
    private final static Logger LOGGER = LoggerFactory.getLogger(App.class.getName());

    public static void main(String[] args) {
        LOGGER.info("Program started...");
        InputStream resourcesInputStream = App.class.getClassLoader().getResourceAsStream("application.properties");
        Properties properties = new Properties();
        try {
            properties.load(resourcesInputStream);
        } catch (IOException e) {
            LOGGER.warn("Cannot read property ", e);
        }
        String dataBaseName = properties.getProperty("db.name");
        String dataBaseURL = properties.getProperty("db.url");
        String dataUserName = properties.getProperty("db.username");
        String dataUserPass = properties.getProperty("db.password");
        String dataBaseConnParam = properties.getProperty("db.connparams");
        LOGGER.debug("DB user {}, DB pass {}, DBname {} ", dataUserName, dataUserPass, dataBaseName);
        LOGGER.info("Program ended...");

//      try {
//        Class.forName("com.mysql.jdbc.Driver"); // old versions
//      } catch (ClassNotFoundException e) {
//        e.printStackTrace();
//      }
      String authorName = "\' some Author \'";
      String dbPath = dataBaseURL+dataBaseName+dataBaseConnParam;
      int author_id_gt = 5;
      int author_id_lt = 10;
      String query = "SELECT * FROM books where b_year > ? AND b_quantity > ?";
      String str = "| %5d | %30s | %5d | %3d |\n";
//      String query3 = "1; DROP MYSQL";
//      String query2 = "SELECT * FROM authors where a_id > 6 AND a_id < ?";
//      String sql0 = "INSERT INTO authors(a_name) values("+ authorName +");";


      try(Connection connection =
            DriverManager.getConnection(dbPath, dataUserName, dataUserPass);
          Statement statement = connection.createStatement();

          CallableStatement cs = connection.prepareCall(query);
      ) {

          LOGGER.info("We are connected !!!");
          connection.setAutoCommit(false);
          try(PreparedStatement statement2 = connection.prepareStatement(query)) {
            statement2.setInt(1, 1990);
            statement2.setInt(2, 1);

            ResultSet resultSet = statement2.executeQuery();

            RowSetFactory rsf = RowSetProvider.newFactory();
            WebRowSet wrs = rsf.createWebRowSet();
            wrs.populate(resultSet);

            try {
              wrs.writeXml(new FileOutputStream("file.xml"));
            } catch (IOException e) {
              e.printStackTrace();
            }


            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            String tableName = resultSetMetaData.getTableName(1);
            System.out.println(tableName);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
              stringBuilder.append("| ");
              stringBuilder.append(resultSetMetaData.getColumnName(i));
              stringBuilder.append(" | ");
            }
            System.out.println(stringBuilder);
            Savepoint point1 = connection.setSavepoint();
            while (resultSet.next()) {
              int bookId = resultSet.getInt("b_id");
              String bookName = resultSet.getString("b_name");
              int bookYear = resultSet.getInt("b_year");
              int bookQuantity = resultSet.getInt("b_quantity");
              System.out.printf(str, bookId, bookName, bookYear, bookQuantity);
            }
            Savepoint point2 = connection.setSavepoint();
            if(false) {
              connection.rollback(point1);
            }
            connection.commit();

          } catch (SQLException s) {
            LOGGER.warn("statement was not executed",s);
              connection.rollback();
          }




//          connection.setAutoCommit(false);
//
//        int inserted =
//          statement.executeUpdate(sql0); // DDL: create, drop, alter;
//                                         // DML: update, insert, delete
////        ResultSet rs = statement.executeQuery(sql0);
//        statement2.setInt(1, author_id_gt);
//        statement2.setInt(2, author_id_lt);
//        ResultSet resultSet = statement2.executeQuery();

//        ResultSet rs = statement.executeQuery("SELECT * FROM authors WHERE a_name like \'%o%\' ORDER BY a_id DESC ;");
//
//
//        rs.absolute(5);
//        while (rs.previous()) {
//          int authorId = rs.getInt("a_id");// 1
//          String authorName2 = rs.getString(2);
////          LOGGER.info("| {} | {} |", authorId, authorName2);
//          System.out.printf("| %3d | %20s |\n", authorId, authorName2);
//        }



//        LOGGER.info("inserted {} values", inserted);
//          connection.commit();
      } catch (SQLException se) {
        LOGGER.warn("cannot connect to SQL Server, exception{}____ code={}", se, se.getErrorCode());
      }

    }
}
