import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;

public class MyApplication extends JFrame {
    public MyApplication(String title) {
        this.setTitle(title);
    }


    private static void setFrameProperties(JFrame frame) {
        Dimension screenSizeDimension = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setSize(screenSizeDimension.width/2, screenSizeDimension.height/2);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(20, 30);
    }

    private static void addComponents(JFrame frame) {
        Container container = frame.getContentPane();

        JPanel panel = new JPanel();
        panel.setBackground(Color.YELLOW);
        panel.setLayout(new FlowLayout());

        JLabel label = new JLabel("Run a program");
        JComboBox<String> comboBox = new JComboBox<>(new String[]{"Calculator", "notepad"});
        JButton button = new JButton("RUN !!!");
        panel.add(label);
        panel.add(comboBox);
        panel.add(button);

        container.add(panel);
    }

    public static void main(String[] args) {
        MyApplication myApplication = new MyApplication("Title of my frame");
        setFrameProperties(myApplication);
        addComponents(myApplication);
//        createMenu(myApplication);

//        myApplication.pack();
    }

    private static JMenuBar createMenu(JFrame frame) {
        JMenuBar menuBar = new JMenuBar();
        Container container = frame.getContentPane();
        JMenu fileMenu = new JMenu("Message");
        JMenu helpMenu = new JMenu("<html><b><u>H</u>elp</b></html>");
        JMenuItem questionMenuItem = new JMenuItem("Question");
        fileMenu.add(questionMenuItem);
        fileMenu.add(new JMenuItem("Input name"));
        fileMenu.addSeparator();
        JMenuItem exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("bye - bye");
                System.exit(0);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);
        helpMenu.add(new JMenuItem("License"));
        helpMenu.add(new JMenuItem("About"));
        menuBar.add(helpMenu);   
        return menuBar;
    }
}
